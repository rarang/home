/**
 * index.js
 All our useful JS goes here, awesome!
 */
$('#user-pw, #user-pw2').on('keyup', function(){
	var $pw = document.getElementById('user-pw'),
		$pw_confirm = document.getElementById('user-pw2');

	if ($pw.value != ""){
		if ($pw.value != $pw_confirm.value) {
			$pw_confirm.setCustomValidity('비밀번호가 일치하지 않습니다.'); //submit 시 팝업
			$pw_confirm.classList.remove('valid');
			$pw_confirm.classList.add('invalid');
		}
		else {
			$pw_confirm.setCustomValidity('비밀번호가 일치합니다.'); //submit 시 팝업
			$pw_confirm.classList.remove('invalid')
			$pw_confirm.classList.add('valid');
		}
	}
});
$('.section-card .card-img').each(function(){
	if (this.naturalWidth > this.naturalHeight){
		$(this).addClass('landscape')
	}
	if (this.naturalWidth < this.naturalHeight){
		$(this).addClass('portrait')
	}
});
function toggle(obj, className) {
	var ctrObj = $(obj).attr('aria-controls');

	if( $(obj).attr('aria-pressed') === "false" ) {
		$(obj).attr('aria-pressed',true);
	}
	else {
		$(obj).attr('aria-pressed',false);
	}
	$('#' + ctrObj).toggleClass(className);
}
function selectCard(obj, className){
	var cardLink = obj.className;
	$('.'+cardLink).removeClass(className);
	$(obj).addClass(className);
}
function addClass(obj, className){
	$(obj).addClass(className);
}
function removeClass(obj, className){
	$(obj).removeClass(className);
}
function toggleClass(obj, className){
	$(obj).toggleClass(className);
}
function popMenu(obj) {
	if( $(obj).attr('aria-haspopup') === "false" ) {
		$(obj).attr('aria-haspopup',true);
		$(obj).attr('aria-expanded',true);
	}
	else {
		$(obj).attr('aria-haspopup',false);
		$(obj).attr('aria-expanded',false);
	}
}

// accordion 메뉴
$('.btn-accordion').on('click', function(){
	var $btn = $(this);
	var $obj = $(this).next('.accordion-content');

	if( $(this).attr('aria-expanded') == 'false' ){
		$(this).attr('aria-pressed','true');
		$obj.attr('aria-hidden','false');

		$obj.slideDown({
			duration : 1000,
			easing : 'easeOutCubic'
		});

		$('body,html').animate({
			scrollTop : $obj.offset().top
		}, 1000, 'easeOutCubic', function(){
			$btn.attr('aria-expanded', 'true');
		});
	}

	else if( $(this).attr('aria-expanded') == 'true'){
		$(this).attr('aria-pressed','false');
		$obj.attr('aria-hidden','true');
		$obj.slideUp({
			duration : 1000,
			easing : 'easeOutCirc',
			complete : function(){
				$btn.attr('aria-expanded', 'false');
			}
		});
	}
	return false;
});


// carousel
var listNum = $('.carousel-list').length;
var current = 1;
var $listContainer = $('.carousel-container');

$('.carousel-next').on('click',function(){
	count('next');
})
$('.carousel-prev').on('click',function(){
	count('prev');
})
$('.carousel-container .num').on('click',function(){
	current = $('.num-list .num').index(this) + 1;
	$listContainer.attr('data-num', current);

	return false;
})

function count(flag){
	if(flag == 'next') {
		if(current == listNum )
			current = 1;
		else
			current++;
	}
	else {
		if(current == 1)
			current = listNum;
		else
			current--;
	}
	$listContainer.attr('data-num', current);
}


//layer
var $layerLink = document.getElementsByClassName('link-layer');
for(var i=0; i<$layerLink.length; i++){
	$layerLink[i].addEventListener('click', function(e){
		this.nextElementSibling.classList.add('on');
		if( this.nextElementSibling.classList.contains("layer") ){
			document.body.classList.add('layer-scroll-block');
		}
		e.preventDefault();
	})
}

// 레이어 공통부분
var $layerDim = document.getElementsByClassName('layer-dim');
var $layerClose = document.getElementsByClassName('layer-close');

for(var i=0; i<$layerDim.length; i++){
	$layerDim[i].addEventListener('click', function(){
		this.parentElement.classList.remove('on');
		document.body.classList.remove('layer-scroll-block');
	})
}
for(var i=0; i<$layerClose.length; i++){
	$layerClose[i].addEventListener('click', function(){
		this.closest('.layer-flow').classList.remove('on');
	})
}
